#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  Card deck[52];
  int i = 0;
  int j = 0;
  int deckCtr = 0;
  //creating a deck with 52 cards
  //the loop goes 52 times, but there are 4 sets of 13 cards for each suit
  for(i = 0; i < 4; i++)
  {
    for(j = 2; j < 15; j++)
    {
      deck[deckCtr].suit = static_cast<Suit>(i);
      deck[deckCtr].value = j;
      deckCtr++;
    }
  }


  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  int (*randPtr)(int) = &myrandom;
  bool (*suit_orderPtr)(const Card&, const Card&) = &suit_order;

  //random shuffle takes the created deck and shuffles it randomly
  //we need the seed to make sure the shuffle is random every time
  random_shuffle(deck, deck + 52, randPtr);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

  //the sort function takes a sorting function and all of the cards and sorts
  //the cards so they can be displayed in order
  sort(hand, hand + 5, suit_orderPtr);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
  for(i = 0; i < 5; i++)
  {
    //displaying the 5 card hand with a width of 10 per line
    //using deck instead of hand will result in no sorting
    cout << setw(10) << get_card_name(hand[i]) << get_suit_code(hand[i])
    << endl;
  }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
//the parameters are two Cards that their suit values are being compared
//if the suits are the same then the Cards number values are compared
bool suit_order(const Card& lhs, const Card& rhs) {
  //we want Hearts to have precedent over all other suits
  //if suit is the same then compare the values
  if( lhs.suit < rhs.suit)
  {
    return true;
  }
  else if(lhs.suit > rhs.suit)
  {
    return false;
  }
  else if(lhs.suit == rhs.suit)
  {
    if(lhs.value < rhs.value)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return true;
  }
}

//this function gets the suit code and returns the unicode value of
//the suit
//the parameter is a Card and its suit value is determined and returned
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//this function gets the name of the card
//if it is a number, the number is converted to a string
//the parameter is a Card and its number value is determined and returned
string get_card_name(Card& c) {
  switch (c.value)
  {
    case 11:  return "Jack of ";
    case 12:  return "Queen of ";
    case 13:  return "King of ";
    case 14:  return "Ace of ";
    default:  return to_string(c.value) + " of ";
  }
}
